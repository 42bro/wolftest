# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/01/15 19:53:43 by almoraru          #+#    #+#              #
#    Updated: 2019/04/10 20:00:14 by almoraru         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

SRC_DIR = srcs

OBJ_DIR = srcs

COLOR = \x1b[1;34m

COLOR2 = \x1b[1;35m

COLOR3 = \x1b[0;32m

COLOR4 = \x1b[0;36m

SRCS = $(SRC_DIR)/main.c $(SRC_DIR)/errors.c $(SRC_DIR)/init.c				\
	$(SRC_DIR)/slav3d_utils.c $(SRC_DIR)/slav3d.c $(SRC_DIR)/ft_mem.c		 \
	$(SRC_DIR)/player.c $(SRC_DIR)/ray_caster.c $(SRC_DIR)/game.c			 \
	$(SRC_DIR)/sdl_events.c $(SRC_DIR)/math.c $(SRC_DIR)/draw.c			   \
	$(SRC_DIR)/render.c $(SRC_DIR)/png.c $(SRC_DIR)/png_utils.c \
	$(SRC_DIR)/png_filters.c

NAME = wolf3d

FLAGS = -Wall -Wextra -Werror -g -fsanitize=address

OBJECTS = $(SRCS:%.c=%.o)

LIB_PATH = libft/

HEADER_PATH = includes/

INCLUDES = $(LIB_PARTH)/ $(HEADER_PATH)/

INCLUDE = -I$(LIB_PATH)/ -I$(HEADER_PATH)/

SDL_LIB = SDL_lib -l SDL2-2.0.0

all: $(NAME)

$(OBJECTS): $(OBJ_DIR)/%.o : $(SRC_DIR)/%.c $(INCLUDES)
	@$(CC) -c $< -o $@ $(FLAGS) $(INCLUDE)

$(NAME): $(OBJECTS)
	@make -C libft
	@$(CC) -o $@ $^ $(FLAGS) -L libft -lft $(INCLUDE) -framework opencl \
	-L $(SDL_LIB) -framework OpenGL -lz
	@echo "$(COLOR)We did it baby!"
clean:
	@echo "$(COLOR3)Cleaning objects!"
	@make clean -C libft
	@rm -f $(OBJECTS)

fclean: clean
	@echo "$(COLOR)Removing :("
	@make fclean -C libft
	@rm -f $(NAME)
	@echo "$(COLOR4)You murderer!"

re: fclean all

.PHONY: all clean fclean re
