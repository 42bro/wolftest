/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 14:49:49 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/08 19:26:10 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	run_game(t_env *e)
{
	long	game_ticks;
	long	time;
	t_ray		r[VIEWPLANE_LENGTH];

	game_ticks = 0;
	while (e->tog.game_is_running)
	{
		time = SDL_GetTicks();
		handle_sdl_events(e);
		//printf("W = %d\nS = %d\nA = %d\nD = %d\n", e->player.move_forward, e->player.move_back, e->player.turn_left, e->player.turn_right);
		update_player(e);
		update_raycaster(e, r);
		render(e, r);
		SDL_Delay(10);
		if (!(game_ticks++ % 500))
			printf("FPS = %.2f\n", 1000.0f / (float)(SDL_GetTicks() - time));
	}
}