/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/26 12:03:01 by almoraru          #+#    #+#             */
/*   Updated: 2019/03/26 12:23:36 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

/*
** Simple error function to quit the program. Displays whatever string you give it
*/

void	ft_error(char *s)
{
	ft_putstr("Error! ");
	ft_putstr(s);
	ft_putendl("Freeing...");
	// TODO(almoraru): free things before exiting
	exit(69);
}