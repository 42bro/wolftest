/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 14:44:17 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/11 20:02:11 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

float	get_undistorted_ray_len(t_3f_vector *ray, t_3f_vector view_plane_dir)
{
	t_3f_vector undistorted_ray;
	t_3f_vector proj;

	proj = vector_proj(ray, &view_plane_dir);
	undistorted_ray = vector_subtract(ray, &proj);

	return (homogeneous_vector_magnitude(&undistorted_ray));
}

float	calculate_draw_height(float ray_lenght, float distance)
{
	return(distance * WALL_SIZE / ray_lenght);
}

int		get_texture_column_nb_for_ray(t_3f_vector *ray, e_raytype rtype, t_env *e)
{
	t_3f_vector ray_hit;

	ray_hit = vector_add(&e->player.pos, ray);
	if (rtype == h_ray)
	{
		if (ray->y < 0)
			return ((int)ray_hit.x % TEXTURE_SIZE);
		else
			return (TEXTURE_SIZE - 1 - ((int)ray_hit.x % TEXTURE_SIZE));
	}
	else
	{
		if (ray->x > 0)
			return ((int)ray_hit.y % TEXTURE_SIZE);
		else
			return (TEXTURE_SIZE - 1 - ((int)ray_hit.y % TEXTURE_SIZE));
	}
}

void	render_proj_scene(t_env *e, t_ray *r)
{
	int i;
	int color;
	int texnum;

	// TODO(almoraru): implement slow render mode;

	i = -1;

	while (++i < WIN_WIDTH)
	{
		e->rend.texture_x = 0;
		if (homogeneous_vector_magnitude(&r[i].h_ray) < homogeneous_vector_magnitude(&r[i].v_ray))
		{
			e->rend.ray = r[i].h_ray;
			e->rend.rtype = h_ray;
			e->rend.coords = get_tile_coord_for_h_ray(&e->rend.ray, e);
			e->rend.map_x = e->rend.coords.x;
			e->rend.map_y = e->rend.coords.y;
			//printf("coord.x = %f\ncoord.y = %f\n", e->rend.coords.x, e->rend.coords.y);
		}
		else
		{
			e->rend.ray = r[i].v_ray;
			e->rend.rtype = v_ray;
			e->rend.coords = get_tile_coord_for_v_ray(&e->rend.ray, e);
			e->rend.map_x = e->rend.coords.x;
			e->rend.map_y = e->rend.coords.y;
		}
		// TODO(almoraru): imlement bonuses, Texture mode, slow render mode, distortion
		/*e->rend.draw_len = calculate_draw_height(e, get_undistorted_ray_len(&e->rend.ray 
 , e->viewplane_dir)); */
		e->rend.draw_len = calculate_draw_height(get_undistorted_ray_len(&e->rend.ray, e->viewplane_dir), e->dist_from_viewplane);
		//printf("draw_len = %f\n", e->rend.draw_len);
		if (e->tog.texture_mode)
		{
			e->rend.texture_x = get_texture_column_nb_for_ray(&e->rend.ray, e->rend.rtype, e);
			texnum = e->map[e->rend.map_y][e->rend.map_x];
			if (texnum < 1 || texnum > 5)
				texnum = 5;
			prep_draw_texture(&e->d,  (WIN_HEIGHT / 2.0f) - (e->rend.draw_len / 2.0f), e->textures[texnum - 1], e->rend.rtype = h_ray);
			//printf("draw = %d|%d\nreal = %d|%d\n", d.texture->w, d.texture->h, e->textures[texnum - 1]->w, e->textures[texnum - 1]->h);
			if (e->d.texture->w != e->textures[texnum - 1]->w || e->d.texture->h != e->textures[texnum -1]->h)
				puts("OH NO!");
			draw_texture(i, e->rend.draw_len, &e->d, e);
		}
		else
		{
			color = e->map[e->rend.map_y][e->rend.map_x];
			if (color < 1 || color > 4)
				color = 4;
			prerp_draw_no_texture(&e->d, (WIN_HEIGHT / 2.0f) - (e->rend.draw_len / 2.0f), e->colors[color - 1], e->rend.rtype = h_ray);
			draw_no_texture(i, e->rend.draw_len, &e->d, e);
		}
	}
	clear_renderer(e);
	slav_draw_full_screen_texture(e, e->screen_buf);
}

void	render(t_env *e, t_ray *r)
{
	if (e->tog.show_map)
	{
		SDL_RenderClear(e->renderer);
		render_map(e, -1, r);
	}
	else
		render_proj_scene(e, r);
}
