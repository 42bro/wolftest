/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 17:50:49 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/11 20:07:12 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	prerp_draw_no_texture(t_draw *d, float wall_y_start, Uint32 argb_color
						   , char darken)
{
	d->wall_y_start = wall_y_start;
	d->argb_color = argb_color;
	d->darken = darken;
}

void	prep_draw_texture(t_draw *d, float wall_y_start, SDL_Surface  *texture, char darken)
{
	d->wall_y_start = wall_y_start;
	d->texture = texture;
	d->darken = darken;
}

void	draw_texture(int x, int len, t_draw *d, t_env *e)
{
	int y;
	float dy;
	float ty;
	Uint32 color;

	y = -1;
	if (d->wall_y_start < 0)
		d->wall_y_start = 0;
	while (++y < WIN_HEIGHT)
	{
		dy = y - (WIN_HEIGHT / 2.0f) + len / 2.0f;
		ty = dy * (float)(TEXTURE_SIZE - EPS) / len;
		if (y < d->wall_y_start)
			e->screen_buf[XY_TO_SCREEN_INDEX(x, y)] = CEILING_COLOR;
		else if (y > (d->wall_y_start + len))
			e->screen_buf[XY_TO_SCREEN_INDEX(x, y)] = FLOOR_COLOR;
		else
		{
			color = ((Uint32*)(d->texture->pixels))[XY_TO_TEXTURE_INDEX(e->rend.texture_x , (int)ty, d->texture->w + d->texture->h)];
			if (d->darken)
				color = DARKEN_COLOR(color);
			e->screen_buf[XY_TO_SCREEN_INDEX(x, y)] = color;
		}
	}
}

void	draw_no_texture(int x, int len, t_draw *d, t_env *e)
{
	int y;

	y = -1;
	if (d->wall_y_start < 0)
		d->wall_y_start = 0;
	while (++y < WIN_HEIGHT)
	{
		if (y < d->wall_y_start)
			e->screen_buf[XY_TO_SCREEN_INDEX(x, y)] = CEILING_COLOR;
		else if (y > (d->wall_y_start + len))
			e->screen_buf[XY_TO_SCREEN_INDEX(x, y)] = FLOOR_COLOR;
		else
			e->screen_buf[XY_TO_SCREEN_INDEX(x, y)] = (d->darken) ? d->argb_color : DARKEN_COLOR(d->argb_color);
	}
}

void	draw_player_line(t_env *e, int map_x_offset, int map_y_offset)
{
	t_point p;

	SDL_SetRenderDrawColor(e->renderer, 100, 100, 255, 255);
	prep_draw_line1(&p, (int)(e->player.pos.x * WIN_HEIGHT / (float)M_PIXEL_W) + map_x_offset
					,(int)(e->player.pos.y * WIN_HEIGHT / (float)M_PIXEL_H) + map_y_offset);
	prep_draw_line2(&p, (int)((e->player.pos.x + PLAYER_SIZE * e->player.dir.x) * WIN_HEIGHT
							  / (float)M_PIXEL_W) + map_x_offset
					,(int)((e->player.pos.y + PLAYER_SIZE * e->player.dir.y) * WIN_HEIGHT
						   / (float)M_PIXEL_W) + map_y_offset);
	//if (e->tog.slow_render_mode)
	// TODO(almoraru): add calculation for slow renderer mode;
	draw_line(e, &p);
	if (e->tog.slow_render_mode)
		e->tog.slow_render_mode = 0;
	SDL_SetRenderDrawColor(e->renderer, 200, 200, 255, 255);
	present_renderer(e);
}

void	render_rays(t_env *e, t_ray *r, int map_x_offset, int map_y_offset)
{
	t_3f_vector ray;
	t_point		p;
	int i;

	i = -1;
	SDL_SetRenderDrawColor(e->renderer, 238, 244, 66, 255);
	while (++i < WIN_WIDTH)
	{
		if (fabs(homogeneous_vector_magnitude(&r[i].h_ray)) < fabs(homogeneous_vector_magnitude(&r[i].v_ray)))
			ray = r[i].h_ray;
		else
			ray = r[i].v_ray;
		prep_draw_line1(&p, (int)(e->player.pos.x * WIN_HEIGHT / (float)M_PIXEL_W) + map_x_offset
						, (int)(e->player.pos.y * WIN_HEIGHT / (float)M_PIXEL_H) + map_y_offset);
		prep_draw_line2(&p, (int)((e->player.pos.x + ray.x) * WIN_HEIGHT / (float)M_PIXEL_W) + map_x_offset
						,(int)((e->player.pos.y + ray.y) * WIN_HEIGHT / (float)M_PIXEL_W) + map_y_offset);
		draw_line(e, &p);
	}
	draw_player_line(e, map_x_offset, map_y_offset);
}

void	render_map(t_env *e, int row, t_ray *r)
{
	float	map_grid_square_size;
	int		map_x_offset;
	int		map_y_offset;
	int		col;
	SDL_Rect	rect;

	map_grid_square_size = (float)WIN_HEIGHT / (float)e->mapw;
	map_x_offset = (WIN_WIDTH - WIN_HEIGHT) / 2;
	map_y_offset = (WIN_HEIGHT - WIN_HEIGHT) / 2;
	while (++row < e->maph)
	{
		col = -1;
		while(++col < e->mapw)
		{
			if (e->map[row][col] == R)
				SDL_SetRenderDrawColor(e->renderer, 155, 0, 0, 255);
			else if (e->map[row][col] == G)
				SDL_SetRenderDrawColor(e->renderer, 0, 155, 0, 255);
			else if (e->map[row][col] == B)

				SDL_SetRenderDrawColor(e->renderer, 0, 0, 155, 255);
			else if (e->map[row][col] == W)
				SDL_SetRenderDrawColor(e->renderer, 128, 128, 128, 255);

			else
				SDL_SetRenderDrawColor(e->renderer, 55, 55, 55, 0);
			rect.x = (int)(map_grid_square_size * col) + map_x_offset;
			rect.y = (int)(map_grid_square_size * row) + map_y_offset;
			rect.w = map_grid_square_size;
			rect.h = map_grid_square_size;
			SDL_RenderFillRect(e->renderer, &rect);
		}
	}
	render_rays(e, r, map_x_offset, map_y_offset);
}
