/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slav3d_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 19:42:54 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/08 19:26:00 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "wolf3d.h"

Uint32 *generatex_texture(int size, t_env *e)
{
	int x;
	int y;
	float factor;
	Uint32 *texture;

	x = -1;
	factor = 256.0f / (float)size;
	texture = create_texture(size, size, e);
	while (++x < size)
	{
		y = -1;
		while(++y < size)
			texture[(size * y) + x] = RGB_TO_ARGB((int)((x ^ y) * factor)&e->redmask
												  , (int)((x ^ y) * factor)&e->greenmask
												  , (int)((x ^ y) * factor)&e->bluemask);
	}
	return (texture);
}

Uint32 *generate_redx_texture(int size, t_env *e)
{
	e->redmask = 0xFF;
	e->greenmask = 0x00;
	e->bluemask = 0x00;
	return (generatex_texture(size, e));
}

Uint32 *generate_greenx_texture(int size, t_env *e)
{
	e->redmask = 0x00;
	e->greenmask = 0xFF;
	e->bluemask = 0x00;
	return (generatex_texture(size, e));
}

Uint32 *generate_bluex_texture(int size, t_env *e)
{
	e->redmask = 0x00;
	e->greenmask = 0x00;
	e->bluemask = 0xFF;
	return (generatex_texture(size, e));
}

Uint32 *generate_grayx_texture(int size, t_env *e)
{
	e->redmask = 0xFF;
	e->greenmask = 0xFF;
	e->bluemask = 0xFF;
	return (generatex_texture(size, e));
}