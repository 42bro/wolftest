/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 15:43:23 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/10 18:17:02 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		is_movement_key(int key)
{
	return (key == SDLK_w || key == SDLK_s || key == SDLK_a || key == SDLK_d);
}

void	wasd(t_env *e)
{
	if (e->event.key.keysym.sym == SDLK_w)
		e->player.move_forward = TRUE;
	else if (e->event.key.keysym.sym == SDLK_s)
		e->player.move_back = TRUE;
	else if (e->event.key.keysym.sym == SDLK_a)
		e->player.turn_left = TRUE;
	else if (e->event.key.keysym.sym == SDLK_d)
		e->player.turn_right = TRUE;
}

void	reeeeee_startmusic(Mix_Music *music)
{
	if (!Mix_PlayingMusic())
		Mix_PlayMusic(music, -1);
	else if (Mix_PausedMusic())
		Mix_ResumeMusic();
	else
	{
		Mix_HaltMusic();
		Mix_PlayMusic(music, -1);
	}
}

void	handle_sdl_events(t_env *e)
{
	Mix_Music	*blyat = Mix_LoadMUS("sounds/blyatradio.mp3");
	Mix_Chunk	*sfx = Mix_LoadWAV("sounds/ak47_pew.wav");

	while (SDL_PollEvent(&e->event))
	{
		if (e->event.type == SDL_KEYDOWN)
		{
			if (e->event.key.keysym.sym == SDLK_ESCAPE)
				e->tog.game_is_running = FALSE;
			else if (is_movement_key(e->event.key.keysym.sym))
				wasd(e);
			else if (e->event.key.keysym.sym == SDLK_m)
			{
				reeeeee_startmusic(blyat);
				e->tog.show_map = !e->tog.show_map;
			}
			else if (e->event.key.keysym.sym == SDLK_z)
				e->dist_from_viewplane -= 20.0f;
			else if (e->event.key.keysym.sym == SDLK_x)
				e->dist_from_viewplane += 20.0f;
			else if (e->event.key.keysym.sym == SDLK_t)
			{
				Mix_PlayChannel(-1, sfx, 0);
				e->tog.texture_mode = !e->tog.texture_mode;
			}
		}
		else if (e->event.type == SDL_WINDOWEVENT)
		{
			;//TODO(fdubois) : put background music here
		}
		else if (e->event.type == SDL_QUIT)
		{
			Mix_FreeChunk(sfx);
			Mix_FreeMusic(blyat);
			Mix_Quit();
			e->tog.game_is_running = FALSE;
		}
	}
}
