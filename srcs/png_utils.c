/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 15:45:18 by fdubois           #+#    #+#             */
/*   Updated: 2019/04/08 13:35:00 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/png.h"

uint32_t	get_chunk_len(unsigned char *tmp)
{
	int i;

	i = 0;
	return ((tmp[i] << 24 | tmp[(i + 1)] << 16 | tmp[(i + 2)] << 8 | tmp[(i + 3)]));
}

void	crc_check(int fd, unsigned char *tmp)
{
	if (read(fd, tmp, 4) < 4)
		gros_fail("Error reading/checking CRC. We totally check it");
}

void	read_for_nuthin(int fd, uint32_t len)
{
	//for chunks we dont care about
	unsigned char	tmp[len];

	if (read(fd, tmp, len) < len)
		gros_fail("invalid chunk !");
}

t_img	png_header_check(int fd)
{
	unsigned char tmp[8];
	t_img img;

	if (read(fd, tmp, 8) < 8 || ft_strncmp((const char*)tmp, "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A", 8))
		gros_fail("Not a PNG.");
	if (read(fd, tmp, 8) < 8 || ft_strncmp((const char*)(tmp + 4), "IHDR", 4))
		gros_fail("Missing or invalid header chunk");
	img = get_png_header_info(fd, get_chunk_len(tmp));
	crc_check(fd, tmp);
	return (img);
}
