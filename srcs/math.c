/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 17:17:48 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/06 18:16:59 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_3f_vector	vector_subtract(t_3f_vector *vec1, t_3f_vector *vec2)
{
	t_3f_vector ret_vec;

	ret_vec.x = vec1->x - vec2->x;
	ret_vec.y = vec1->y - vec2->y;
	ret_vec.z = 1;
	return (ret_vec);
}

float		homogeneous_vector_magnitude(t_3f_vector *vec)
{
	return (sqrt(vec->x * vec->x + vec->y * vec->y));
}

t_3f_vector	homogeneous_v_scale(t_3f_vector *vec, float scale)
{
	t_3f_vector ret_vec;

	ret_vec.x = vec->x * scale;
	ret_vec.y = vec->y * scale;
	ret_vec.z = 1;
	return (ret_vec);
}

t_3f_vector	normalize_vec(t_3f_vector *vec)
{
	return (homogeneous_v_scale(vec, 1.0f / homogeneous_vector_magnitude(vec)));
}

void	matrix_vector_multiply(t_matrix3f *mat, t_3f_vector *vec)
{
	t_3f_vector new_vec;

	new_vec.x = (*mat)[0][0] * vec->x + (*mat)[0][1] * vec->y + (*mat)[0][2] * vec->z;
	new_vec.y = (*mat)[1][0] * vec->x + (*mat)[1][1] * vec->y + (*mat)[1][2] * vec->z;
	new_vec.z = (*mat)[2][0] * vec->x + (*mat)[2][1] * vec->y + (*mat)[2][2] * vec->z;
	*vec = new_vec;
}

float	vector_point_product(t_3f_vector *vec1, t_3f_vector *vec2)
{
	return (vec1->x * vec2->x + vec1->y * vec2->y);
}

t_3f_vector	vector_add(t_3f_vector *vec1, t_3f_vector *vec2)
{
	t_3f_vector ret_vec;
	ret_vec.x = vec1->x + vec2->x;
	ret_vec.y = vec1->y + vec2->y;
	ret_vec.z = 1;
	return (ret_vec);
}

t_3f_vector	vector_proj(t_3f_vector *vec1, t_3f_vector *vec2)
{
	t_3f_vector p_vec;

	p_vec = normalize_vec(vec2);
	return (homogeneous_v_scale(&p_vec, vector_point_product(&p_vec, vec1)));
}
