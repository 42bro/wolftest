/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_caster.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 16:55:59 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/08 19:19:30 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

// TODO(almoraru):  split this into 2 files or more, I have a lot of functions here HAHAHAHAH

void	init_ray_direction(t_env *e, t_ray *r)
{
	int i;
	t_3f_vector v1;
	t_3f_vector v2;
	t_3f_vector v3;

	i = -1;
	while (++i < VIEWPLANE_LENGTH)
	{
		v1 = homogeneous_v_scale(&e->player.dir, e->dist_from_viewplane);
		v2 = homogeneous_v_scale(&e->viewplane_dir, ((VIEWPLANE_LENGTH / 2) - i));
		v3 = vector_subtract(&v1, &v2);
		r[i].v_ray = normalize_vec(&v3);
		r[i].h_ray = normalize_vec(&v3);
		//i, r[i].v_ray.x, r[i].v_ray.y, r[i].v_ray.z);
		//printf("ok then %f|%f|%f\nand this %f|%f|%f\n", r[i].v_ray.x, r[i].v_ray.y, r[i].v_ray.z
		//, r[i].h_ray.x, r[i].h_ray.y, r[i].h_ray.z);
		/*if (e->tog.ray_cast_mode == ONLY_NORMAL)
  { 
   r[i].h_ray  = homogeneous_v_scale(&r[i].h_ray, 40);
   r[i].v_ray  = homogeneous_v_scale(&r[i].v_ray, 40);
  } */
	}
}

t_3f_vector	find_h_ray_step_vec(t_3f_vector *ray)
{
	t_3f_vector step_vec;

	step_vec.x = 0;
	step_vec.y = 0;
	step_vec.z = 1;
	if (ray->y < 0)
	{
		/* Ray is facing up */
		step_vec.y = -1 * WALL_SIZE;
	}
	else
	{
		/* Ray is facing down */
		step_vec.y = WALL_SIZE;
	}
	return (homogeneous_v_scale(ray, vector_point_product(&step_vec, &step_vec)
								/ MAKE_FLOAT_EPS(vector_point_product
												 (&step_vec, ray))));
}

t_3f_vector	find_v_ray_step_vec(t_3f_vector *ray)
{
	t_3f_vector step_vec;

	step_vec.x = 0;
	step_vec.y = 0;
	step_vec.z = 1;
	if (ray->x < 0)
	{
		/* Ray is facing left */
		step_vec.x = -1 * WALL_SIZE;
	}
	else
	{
		/* Ray is facing right */
		step_vec.x = WALL_SIZE;
	}
	return (homogeneous_v_scale(ray, vector_point_product(&step_vec, &step_vec)
								/ MAKE_FLOAT_EPS(vector_point_product
												 (&step_vec, ray))));
}

t_3f_vector	get_tile_coord_for_v_ray(t_3f_vector *ray, t_env *e)
{
	t_3f_vector pos;
	t_3f_vector coord;

	pos = vector_add(&e->player.pos, ray);
	coord.x = (int)(pos.x + ((ray->x < 0) ? (-1 * RAY_EPS) : (RAY_EPS))) / WALL_SIZE;
	coord.y = (int)(pos.y + ((ray->y < 0) ? (-1 * EPS) : (EPS))) / WALL_SIZE;

	return (coord);
}

t_3f_vector	get_tile_coord_for_h_ray(t_3f_vector *ray, t_env *e)
{
	t_3f_vector pos;
	t_3f_vector coord;

	pos = vector_add(&e->player.pos, ray);
	coord.x = (int)(pos.x + ((ray->x < 0) ? (-1 * EPS) : EPS)) / WALL_SIZE;
	coord.y = (int)(pos.y + ((ray->y < 0) ? (-1 * RAY_EPS) : (RAY_EPS))) / WALL_SIZE;

	return (coord);
}

void	ray_cast(t_env *e, t_ray *r, int i)
{
	t_ray_cast ray;

	while (++i < VIEWPLANE_LENGTH)
	{
		ray.v_norm = normalize_vec(&r[i].v_ray);
		ray.h_norm = normalize_vec(&r[i].h_ray);
		ray.v_step = find_v_ray_step_vec(&ray.v_norm);
		ray.h_step = find_h_ray_step_vec(&ray.h_norm);
		/* cast the vertical ray until it hits an object */
		ray.map_coord = get_tile_coord_for_v_ray(&r[i].v_ray, e);
		//printf("x = %f\ny = %f\n", ray.map_coord.x, ray.map_coord.y);
		while (ray.map_coord.x > 0 && ray.map_coord.y > 0 
			   && ray.map_coord.x < e->mapw
			   && ray.map_coord.y < e->maph 
			   && e->map[(int)ray.map_coord.y][(int)ray.map_coord.x] < 1)
		{
			r[i].v_ray = vector_add(&r[i].v_ray, &ray.v_step);
			ray.map_coord = get_tile_coord_for_v_ray(&r[i].v_ray, e);
		}
		//same thing for horizontal rays
		ray.map_coord = get_tile_coord_for_h_ray(&r[i].h_ray, e);
		while (ray.map_coord.x > 0 && ray.map_coord.y > 0 
			   && ray.map_coord.x < e->mapw
			   && ray.map_coord.y < e->maph
			   && e->map[(int)ray.map_coord.y][(int)ray.map_coord.x] < 1)
		{
			r[i].h_ray = vector_add(&r[i].h_ray, &ray.h_step);
			ray.map_coord = get_tile_coord_for_h_ray(&r[i].h_ray, e);
		}
	}
}

void	extend_rays_to_first_hit(t_env *e, t_ray *r)
{
	int i;
	t_3f_vector vec;

	//printf("vec = %f|%f|%f\n", vec.x, vec.y, vec.z);
	i = -1;
	while (++i < VIEWPLANE_LENGTH)
	{
		/*  Extend vertical Ray */
		//printf("r[%d] = %f\n", i, r[i].v_ray.x);
		//printf("player pos = %f\n", e->player.pos.x);
		vec.x = 0.0f;
		vec.y = 0.0f;
		vec.z = 1.0f;
		if (r[i].v_ray.x < 0)
		{
			/* Ray is facing left */
			vec.x = ((int)(e->player.pos.x / (float)WALL_SIZE)) * WALL_SIZE - e->player.pos.x;
		}
		else
		{
			/* Ray is facing right */
			vec.x = ((int)(e->player.pos.x / (float)WALL_SIZE)) * WALL_SIZE - e->player.pos.x + WALL_SIZE;
		}
		//printf("vec.x = %f\n", vec.x);
		//printf("v_r[i] = %f|%f|%f\n", r[i].v_ray.x,r[i].v_ray.y,r[i].v_ray.z);

		r[i].v_ray = homogeneous_v_scale(&r[i].v_ray, vector_point_product(&vec, &vec)
										 / MAKE_FLOAT_EPS(vector_point_product(&vec, &r[i].v_ray)));
		//printf("what is the product %f\n", vectorDotProduct(&vec, &vec));
		/* now for horizontal */
		vec.x = 0.0f;
		if (r[i].h_ray.y < 0)
		{
			/* ray is facing up*/
			vec.y = ((int)(e->player.pos.y / (float)WALL_SIZE)) * WALL_SIZE - e->player.pos.y;
		}
		else
		{
			/* Ray is facing down*/
			vec.y = ((int)(e->player.pos.y / (float)WALL_SIZE)) * WALL_SIZE - e->player.pos.y + WALL_SIZE;
		}
		r[i].h_ray = homogeneous_v_scale(&r[i].h_ray, vector_point_product(&vec, &vec)
										 / MAKE_FLOAT_EPS(vector_point_product(&vec, &r[i].h_ray)));
		//printf("h_r[i] = %f|%f|%f\n", r[i].h_ray.x,r[i].h_ray.y,r[i].h_ray.z);
	}
}

void	update_raycaster(t_env *e, t_ray *r)
{
	init_ray_direction(e, r);
	if (e->tog.ray_cast_mode == ONLY_NORMAL)
		return ;
	extend_rays_to_first_hit(e, r);
	if (e->tog.ray_cast_mode == ONLY_FIRST_HIT)
		return ;
	ray_cast(e, r, -1);
}

void	init_rotations(t_matrix3f counter_clock_rotation, t_matrix3f clock_wise_rotation)
{
	counter_clock_rotation[0][0] = 1;
	counter_clock_rotation[0][1] = 0;
	counter_clock_rotation[0][2] = 0;
	counter_clock_rotation[1][0] = 0;
	counter_clock_rotation[1][1] = 1;
	counter_clock_rotation[1][2] = 0;
	counter_clock_rotation[2][0] = 0;
	counter_clock_rotation[2][1] = 0;
	counter_clock_rotation[2][2] = 1;
	clock_wise_rotation[0][0] = 1;
	clock_wise_rotation[0][1] = 0;
	clock_wise_rotation[0][2] = 0;
	clock_wise_rotation[1][0] = 0;
	clock_wise_rotation[1][1] = 1;
	clock_wise_rotation[1][2] = 0;
	clock_wise_rotation[2][0] = 0;
	clock_wise_rotation[2][1] = 0;
	clock_wise_rotation[2][2] = 1;
}

void	init_matrix(t_env *e)
{
	init_rotations(e->counter_clock_rotation, e->clock_wise_rotation);
	printf("CCR1 = %f|%f|%f\nCCR2 = %f|%f|%f\nCCR3 = %f|%f|%f\n"
		   , e->counter_clock_rotation[0][0], e->counter_clock_rotation[0][1]
		   , e->counter_clock_rotation[0][2], e->counter_clock_rotation[1][0]
		   , e->counter_clock_rotation[1][1], e->counter_clock_rotation[1][2]
		   , e->counter_clock_rotation[2][0], e->counter_clock_rotation[2][1]
		   , e->counter_clock_rotation[2][2]);
	printf("CwR1 = %f|%f|%f\nCwR2 = %f|%f|%f\nCwR3 = %f|%f|%f\n"
		   , e->clock_wise_rotation[0][0], e->clock_wise_rotation[0][1]
		   , e->clock_wise_rotation[0][2], e->clock_wise_rotation[1][0]
		   , e->clock_wise_rotation[1][1], e->clock_wise_rotation[1][2]
		   , e->clock_wise_rotation[2][0], e->clock_wise_rotation[2][1]
		   , e->clock_wise_rotation[2][2]);
	/* Infer viewplane distance from a given field of view angle */
	e->dist_from_viewplane = (WIN_WIDTH / 2.0f) / (float)(tan(FOV / 2.0f));

	/* Setup for the player rotation matrices. Basically initializing*/
	e->counter_clock_rotation[0][0] = cos(PLAYER_ROT_SPEED);
	e->counter_clock_rotation[0][1] = -1.0f * sin(PLAYER_ROT_SPEED);
	e->counter_clock_rotation[1][0] = sin(PLAYER_ROT_SPEED);
	e->counter_clock_rotation[1][1] = cos(PLAYER_ROT_SPEED);
	e->clock_wise_rotation[0][0] = cos(-1.0f * PLAYER_ROT_SPEED);
	e->clock_wise_rotation[0][1] = -1.0f * sin(-1.0f * PLAYER_ROT_SPEED);
	e->clock_wise_rotation[1][0] = sin(-1.0f * PLAYER_ROT_SPEED);
	e->clock_wise_rotation[1][1] = cos (-1.0f * PLAYER_ROT_SPEED);
}
