#include <stdlib.h>
#include <stdio.h>
int main(int ac, char **av)
{
    unsigned char tmp[8];
    unsigned char len[4];
    unsigned short chunk_len;
    int i;
    int first;
    i = -1;
    chunk_len = 0;
    first = 0;
    if (ac != 2 || read(0, &tmp, 0) < 0)
        return (1);
    read(0, &tmp, 8);
    if (ft_strncmp(tmp, "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A", 8))
        return (1); // PNG Signature
    while (read(0, &tmp, 8) == 8) 
    {
        // chunk reading, first 4 bytes of chunk length (max length is 2^16 : 65536 bytes)
        int pow = 16;
        while (++i < 4 && pow /= 2)
            chunk_len += (tmp[i] == '1' ? 1 : 0) * pow;
        i = -1;
        // 4 bytes of  chunk type
        if (!first)
            first = 1;
        if (first && ft_strncmp(tmp + 4, "IHDR", 4)) // always first chunk
            return (1);
        else if (!ft_strncmp(tmp + 4, "PLTE", 4)) // color palette
            do_shit();  
        else if (!ft_strncmp(tmp + 4, "IDAT", 4)) // actual image data
            do_shit();
        else if (!ft_strncmp(tmp + 4, "IEND", 4)) // end of data
            do_shit();
        // read chunk data
        unsigned char chunkdata[chunk_len];
        if (read(0, &chunkdata, chunk_len) != chunk_len)
            return (1);
        put_chunk_data_in_SDL_ez();
        // read and check CRC if we give a fuck 
        if (read(0, &tmp, 4) != 4)
            return(1);
        check_crc_if_we_give_a_fuck();
    }
}