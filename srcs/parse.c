#include "wolf3d.h"

typedef struct	s_map
{
	signed char	**m;
	uint32_t	w;
	uint32_t	h;
}				t_map;

typedef struct	s_li
{
	struct s_li *next;
	signed char	*str;
}				t_li;

t_li	*new_node(signed char *str)
{
	t_li	*node;

	if (!(node = (t_li*)malloc(sizeof(t_li))))
		return (NULL);
	node->str = str;
	node->next = NULL;
	return (node);
}

void	add_node(t_li **list, t_li *node)
{
	t_li	*tmp;

	tmp = *list;
	if (!node)
		return ;
	if (!(*list))
		*list = node;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
	}
}

void	free_node(t_li **node)
{
	(*node)->next = NULL;
//	ft_strdel((char**)&((*node)->str));
	ft_memdel((void**)node);
}

int		list_size(t_li *list)
{
	int	i;

	i = 0;
	if (list)
	{
		while (list)
		{
			list = list->next;
			i++;
		}
	}
	return (i);
}

void	free_list(t_li **list)
{
	if ((*list))
	{
		free_list(&((*list)->next));
		free_node(list);
	}
}

void	print_list(t_li *list)
{
	if (!list)
		return ;
	while (list)
	{
		ft_putendl((char*)list->str);
		list = list->next;
	}
}

void	get_map_value(signed char *s)
{
	if (*s == 'P')
		*s = -1;
	else if (*s == 'R')
		*s = 1;
	else if (*s == 'F')
		*s = 0;
	else if (*s == 'G')
		*s = 2;
	else if (*s == 'B')
		*s = 3;
	else if (*s == 'W')
		*s = 4;
	else if (*s == 'A')
		*s = 5;
}

void	fill_tab(t_env *e, t_li *list)
{
	t_li	*tmp;
	int		i;
	int		j;
	
	tmp = list;
	i = -1;
	if (!list)
		return ;
	while (tmp)
	{
		e->map[++i] = tmp->str;
		j = -1;
		while (++j < e->mapw)
			get_map_value(&e->map[i][j]);
		tmp = tmp->next;
	}
}

void	print_tab(signed char **tab, int w, int h)
{
	int	i;
	i = -1;

	while (++i < h)
	{
		write(1, tab[i], w);
		write(1, "\n", 1);
	}
}

void	parse(t_env *e, int ac, char **av)
{
	int		fd;
	char *line;
	int i;
	int lines;
	int sp;
	t_li	*list;

	lines = 0;
	sp = 0;
	list = NULL;
	if (ac != 2 || !(av[1][0]) || ((fd = open(av[1], O_RDONLY)) < 0))
		ft_error("invalid map file.\n usage: ./wolf3d [map file]");
	while (get_next_line(fd, &line) == 1)
	{
		i = -1;
		while (line[++i])
		{
		//	if (i % 2 && line[i] != ' ')
		//		ft_error("invalid map.");
		//	else if (!(i % 2))
			if (line[i] == 'P')
				(sp ? ft_error("invalid map: several start points in map.") : sp++);
			else if (!ft_strchr("ABCDEFGHIJKRSVWXYZ", line[i])) // put all used map chars here
			{
				write(1, line + i, 1);
				ft_error("invalid map: bad characters.");
			}
			
		}
		if (!e->maph)
			e->mapw = ft_strlen(line);
		else if (lines && ((size_t)e->mapw != ft_strlen(line)))
			ft_error("invalid map: lines need to have the same length");
		add_node(&list, new_node((signed char*)line));
		e->maph++;
	}
//	ft_strdel(&line);
	write(1, "e", 1);
	if (!sp)
		ft_error("invalid map: no player starting point.");
	print_list(list);
	if (!(e->map = (signed char**)malloc(sizeof(signed char*) * e->maph)))
		ft_error("malloc sucks ! couldn't allocate the map");
	i = -1;
	fill_tab(e, list);
	print_tab(e->map, e->mapw, e->maph);
	free_list(&list);
	
	
	close(fd);
}
