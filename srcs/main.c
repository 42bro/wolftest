/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 15:54:51 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/11 17:42:00 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"


/*
** This is a temporary map. I will code the game in here. This is an example of 
** how a map will look in the future \[T]/
*/
const int MAP[MAP_GRID_HEIGHT][MAP_GRID_WIDTH] = {
    {R,R,R,R,R,R,R,R,R,R},
    {R,B,0,0,0,0,P,0,B,R},
    {R,0,0,0,0,0,0,0,0,R},
    {R,0,0,G,0,0,G,0,0,R},
    {R,0,0,0,A,0,0,0,0,R},
    {R,0,0,0,0,0,0,0,0,R},
    {R,0,0,G,0,0,G,0,0,R},
    {R,0,0,0,0,0,0,0,0,R},
    {R,B,0,0,0,0,0,0,B,R},
	{R,R,R,R,R,R,R,R,R,R}
};
//is that a global variable ????? you savage

/*
** Strated working properly. Cleaned up my mess.
*/
int		main(int ac, char **av)
{
	t_env	e;
	t_mem	mem;
	int		m_bytes;

	ft_bzero(&e, sizeof(e));
	ft_bzero(&mem, sizeof(mem));
	parse(&e, ac, av);
	m_bytes = 16 * 1024 * 1024; // 16 megabytes
	if( !(mem.m = ft_memalloc(m_bytes)))
		ft_error("Memory block failed to allocate!"); /* Memory block init*/
	mem.tsize = m_bytes; //total size of the block;
	mem.usize = 0; // Used size of the block, this way we know where we are. (useful in case we wanna overwrite memory)
	e.mem = &mem;  /*giving the pointer to the env struct so I can carry it anywhere */
	if(!(ft_init(&e)))
		ft_error("Could not initialize the game!");
	// TODO(almoraru): add more key events in SDL_events then free evrything before quitting  Check out why no proper colors are made ?
	init_player(&e.player, &e);
	init_matrix(&e);
	run_game(&e);
	//printf("-mem used = %zu\nmem total = %zu", e.mem->usize, e.mem->tsize); //I'm keeping this to see how much memory I use.
	return (0);
}
