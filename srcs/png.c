/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/23 02:53:21 by fdubois           #+#    #+#             */
/*   Updated: 2019/04/11 18:36:41 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"
#include "../includes/png.h"
#include <stdio.h>

void	gros_fail(char *str)
{
	ft_putendl(str);
	//TODO:	free_mem();
	exit(1);
}

void	unfilter(unsigned char *str, int i, t_img img, uint8_t filter_type)
{
	if (filter_type == 1)
		sub_unfilter(str, i, img);
	else if (filter_type == 2)
		up_unfilter(str, i, img);
	else if (filter_type == 3)
		average_unfilter(str, i, img);
	else if (filter_type == 4)
		paeth_unfilter(str, i, img);
	else
		return ;
}

unsigned char	*inflator(unsigned char *raw, uint32_t chunk_len, size_t usize)
{
	unsigned char *str;
	str = (unsigned char*)malloc(usize + 1);
	z_stream z;

	z.zalloc = Z_NULL;
	z.zfree = Z_NULL;
	z.opaque = Z_NULL;

	z.avail_in = chunk_len;
	z.next_in = (Bytef*)raw;
	z.avail_out = usize;
	z.next_out = (Bytef*)str;

	inflateInit2_(&z, 15, ZLIB_VERSION, sizeof(z));
	inflate(&z, Z_NO_FLUSH);
	inflateEnd(&z);
	return (str);
}

void	unzip_img_data(unsigned char *raw_data, uint32_t chunk_len, t_img img, uint32_t *pixel)
{
	size_t i;
	int k;
	size_t usize;
	unsigned char *str;
	int filter_type;
	usize = img.h * img.linesize;
	str = inflator(raw_data, chunk_len, usize);
	i = 0;
	k = -1;

	while (i < usize)
	{
		if (!(i % img.linesize))
		{
			filter_type = str[i];
			//	printf("filter byte %hhu \n", str[i]);
			i++;
		}
		else
		{
			unfilter(str, i, img, filter_type);
			//		printf("R : %hhu G : %hhu B : %hhu A %hhu \n", str[i], str[(i + 1)], str[(i + 2)], str[(i + 3)]);fflush(stdout);
			if (img.color_type == 2)
				pixel[++k] = (255 << 24) | (str[i] << 16) | (str[(i + 1)] << 8) | (str[(i + 2)]);
			else if (img.color_type == 3)
				pixel[++k] = (255 << 24) | img.plte[(str[i])];
			else
				pixel[++k] = (str[(i + 3)] << 24) | (str[i] << 16) | (str[(i + 1)] << 8) | (str[(i + 2)]);
			i += (img.bpp);
		}
	}
	ft_strdel((char**)&str);
}

unsigned char	*zipped_data(unsigned char *tmp, int fd, uint32_t *comp_length)

{
	unsigned char *str;
	uint32_t chunk_len;
	unsigned char *chunkdata;
	int i;

	*comp_length = 0;
	str = (unsigned char*)ft_strnew(MAX_IDAT_SIZE); // pls protecc
	i = 0;
	while (!ft_strncmp((const char*)(tmp + 4), "IDAT", 4))
	{
		chunk_len = get_chunk_len(tmp);
		//	printf("%.4s chunklen %d\n", (tmp + 4) , chunk_len);
		chunkdata = (unsigned char*)malloc(sizeof(char) * (chunk_len + 1));
		ft_bzero(chunkdata, chunk_len + 1);
		if (read(fd, chunkdata, chunk_len) != chunk_len)
			return (NULL);
		//p = str;
		ft_memcpy(str + i, chunkdata, chunk_len);
		i += chunk_len;
		//		str = chunkdata;
		*comp_length += chunk_len;
		if (*comp_length >= MAX_IDAT_SIZE)
			gros_fail("PNG file too big !");
		//	printf("compd length = %d \n", *comp_length);
		crc_check(fd, tmp); //who gives a shit about CRC corruption is everywhere
		read(fd, tmp, 8); //read next chunk 8 header bytes (4 of length, 4 of type)
		//	printf("next chunk : %.4s - length %d \n", (tmp + 4), get_chunk_len(tmp));
		ft_memdel((void**)&chunkdata);
	}
	while (ft_strncmp((const char *)(tmp + 4), "IEND", 4))
	{
		read_for_nuthin(fd, get_chunk_len(tmp));
		crc_check(fd, tmp);
		if (read(fd, tmp, 8) != 8)
			gros_fail("Non!!!!!!!!");
	}
	if (ft_strncmp((const char *)(tmp + 4), "IEND", 4))
		gros_fail("Invalid IEND chunk");
	else
		write(1, (tmp + 4), 4);
	return (str);
}

int	*get_palette_info(unsigned char *str, int fd)
{
	//SDL has a palette format (SDL_Palette) which we can use or not
	int *tab;
	unsigned char rgb[4];
	int len;
	int	i;

	len = get_chunk_len(str);
	i = -1;
	if (len % 3)
		return (NULL);
	if (!(tab = (int*)malloc(sizeof(int) * (len / 3))))
		return (NULL);
	while (++i < (len / 3))
	{
		read(fd, rgb, 3);
		tab[i] = (rgb[0] << 16) | (rgb[1] << 8) | (rgb[2]);
		//////	printf("%.6x < palette index %d ,", tab[i], i);fflush(stdout);
	}
	return (tab);
}

t_img	get_png_header_info(int fd, int len)
{
	t_img img;
	unsigned char data[len];

	if (read(fd, data, len) != len)
		gros_fail("Error reading header chunk data");
	img.w = get_chunk_len(data);
	img.h = get_chunk_len(data + 4);
	img.depth = *(data + 8);
	if (img.depth % 2 && img.depth != 1)
		gros_fail("Invalid bit depth");
	img.color_type = *(data + 9);
	img.compression = *(data + 10);
	img.filter = *(data + 11);
	img.interlaced = *(data + 12);
	if (img.interlaced)
		ft_putendl("WARNING : PNG is interlaced (unsupported at this time)");
	if (img.color_type == 6)
		img.channels = 4;
	else if (img.color_type == 4)
		img.channels = 2;
	else
		img.channels = (!img.color_type || img.color_type == 3 ? 1 : 3);
	img.bpp = (img.depth / 8) * img.channels;
	img.linesize = 1 + (img.w * img.bpp);
		printf("\n w %d h %d depth %hhu ctype %hhu comp %hhu filter %hhu interlaced %hhu linesize %u bpp %hhu channels %d \n ", img.w, img.h, img.depth, img.color_type, img.compression, img.filter, img.interlaced, img.linesize, img.bpp, img.channels);fflush(stdout);
	return (img);
}

t_img	import_png_to_surface(int fd, uint32_t *pixels, t_img img)
{
	unsigned char	tmp[8];
	uint32_t		comp_length; //size of the compressed image data
	unsigned char	*zip; //

	if (!fd || !pixels)
		gros_fail("Non.");
	if (read(fd, tmp, 8) < 8)
		gros_fail("Error reading chunk");
	while (ft_strncmp((const char*)(tmp + 4), "IDAT", 4)) //auxiliary chunks come before actual image data (IDAT chunk(s))
	{
		if (!ft_strncmp((const char*)(tmp + 4), "PLTE", 4))
			img.plte = get_palette_info(tmp, fd);
		else
		{
			read_for_nuthin(fd, get_chunk_len(tmp));
			printf("%.4s chunk was skipped \n", (tmp + 4));
		}
		crc_check(fd, tmp);
		if (read(fd, tmp, 8) < 8)
			gros_fail("Error reading chunk");
	}
	if (ft_strncmp((const char*)(tmp + 4), "IDAT", 4))
		gros_fail("Error reading image data");
	zip = zipped_data(tmp, fd, &comp_length);
	unzip_img_data(zip, comp_length, img, pixels);
	ft_memdel((void**)&zip);
	//	ft_memset(zip, 1, comp_length); //need to memdel bc of zeroes everywhere
	//	ft_strdel((char**)&zip);
	close(fd);
	return (img);
}

SDL_Surface *png2surf(char *path)
{
	int fd;
	//	SDL_Window *pwin;
	//	SDL_Renderer *pren;
	SDL_Surface *surf;
	t_img img;

	if (((fd = open(path, O_RDONLY)) == -1) || !path)
		perror("Couldn't open PNG file.");
	img = png_header_check(fd); // checks and gets PNG header info
	if (!(surf = SDL_CreateRGBSurface(0, img.w, img.h, (img.depth * 4), 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000))) // RGBA Masks
		perror(SDL_GetError());
	ft_putendl(path);
	img = import_png_to_surface(fd, surf->pixels, img);
	//	SDL_RenderPresent(pren);
	//	SDL_Delay(1000);
	return (surf);
	//them leaks tho
}
