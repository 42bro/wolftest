/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c <srcs>                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/28 16:02:16 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/08 18:53:40 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		clip_check(t_player *p, float dx, float dy)
{
	float newx;
	float newy;
	int i;
	int j;

	newx = p->pos.x + dx;
	newy = p->pos.y + dy;
	p->x1 = (newx - PLAYER_SIZE) / WALL_SIZE;
	p->y1 = (newy - PLAYER_SIZE) / WALL_SIZE;
	p->x2 = (newx + PLAYER_SIZE) / WALL_SIZE;
	p->y2 = (newy + PLAYER_SIZE) / WALL_SIZE;
	i = p->y1 - 1;
	while (++i <= p->y2)
	{
		j = p->x1 - 1;
		while (++j <= p->x2)
		{
			if (i < 0 || j < 0 || i > p->maph 
				|| j > p->mapw || p->map[i][j] > 0)
				return (TRUE);
		}
	}
	return (FALSE);
}

void	move_player(t_player *p, float dx, float dy)
{
	if (!(clip_check(p, dx, dy)))
	{
		p->pos.x += dx;
		p->pos.y += dy;
		return ;
	}
	if (!(clip_check(p, 0.0f, dy)))
	{
		p->pos.y += dy;
		return ;
	}
	if (!(clip_check(p, dx, 0.0f)))
	{
		p->pos.x += dx;
		return ;
	}
}

void	rotate_player(t_matrix3f *rot_matrix, t_env *e)
{
	matrix_vector_multiply(rot_matrix, &e->player.dir);
	matrix_vector_multiply(rot_matrix, &e->viewplane_dir);
}

void	update_player(t_env *e)
{
	e->player.move_speed = PLAYER_MOVEMENT_SPEED;
	if (e->player.is_running)
		e->player.move_speed *= 2;
	else if (e->player.move_forward)
	{
		move_player(&e->player, e->player.dir.x * e->player.move_speed, e->player.dir.y * e->player.move_speed);
		e->player.move_forward = FALSE;
	}
	else if (e->player.move_back)
	{
		move_player(&e->player , -1 * e->player.dir.x * e->player.move_speed, -1 * e->player.dir.y * e->player.move_speed);
		e->player.move_back = FALSE;
	}
	else if (e->player.turn_left)
	{
		rotate_player(&e->clock_wise_rotation, e);
		e->player.turn_left = FALSE;
	}
	else if (e->player.turn_right)
	{
		rotate_player(&e->counter_clock_rotation, e);
		e->player.turn_right = FALSE;
	}
}

void	init_player(t_player *p, t_env *e)
{
	int row;
	int col;

	p->pos.x = 2.5f * WALL_SIZE; //Normally these would be global
	p->pos.y = 2.5f * WALL_SIZE;
	p->pos.z = 1;
	p->dir.x = 0; // Player direction must be perpendicular to view plane or we can't see shit.
	p->dir.y = 1;
	p->dir.z = 1;
	p->map = e->map;
	p->mapw = e->mapw;
	p->maph = e->maph;

	row = -1;
	//printf("pos x = %f\npos y = %f\n", p->pos.x, p->pos.y);a
	while (++row < e->maph)
	{
		col = -1;
		while (++col < e->mapw)
		{
			if (e->map[row][col] == P)
			{
				p->pos.x = (WALL_SIZE * col) + (WALL_SIZE / 2.0f);
				p->pos.y = (WALL_SIZE * row) + (WALL_SIZE / 2.0f);
				break;
			}
		}
	}
}
