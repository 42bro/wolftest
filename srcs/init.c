/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/26 12:37:47 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/11 19:52:10 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	init_togs(t_env *e)
{
	e->tog.game_is_running = TRUE;
	e->tog.show_map = TRUE;
	e->tog.distortion = FALSE;
	e->tog.slow_render_mode = FALSE;
	e->tog.ray_cast_mode = FALSE;
	e->tog.texture_mode = FALSE;
	e->viewplane_dir.x = VIEWPLANE_DIR_X;
	e->viewplane_dir.y = VIEWPLANE_DIR_Y;
	e->viewplane_dir.z = 1;
}

int	init_win(char *title, unsigned int width, unsigned int height, t_env *e)
{
	if (e->win || e->renderer)
		return(FALSE);
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
		ft_error("Failed to initialize SDL");
	e->win = SDL_CreateWindow(title, 50, 50, width, height, SDL_WINDOW_OPENGL);
	e->renderer = SDL_CreateRenderer(e->win, -1, SDL_RENDERER_ACCELERATED);
	if (!e->win || !e->renderer)
		ft_error("Failed to make window");
	if (Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 2048))
		Mix_GetError();
	SDL_SetWindowTitle(e->win, title);
	SDL_SetRenderDrawColor(e->renderer, 255, 255, 255, 255);
	SDL_RenderClear(e->renderer);
	SDL_RenderPresent(e->renderer);

	return (TRUE);
}
/*
**--------------------------------------------------------------------------------------
** Simple I just initialize everything and color the window Gray.
**--------------------------------------------------------------------------------------
** That is until I create the Texture Data \[T]/!!
**--------------------------------------------------------------------------------------
*/
int	ft_init(t_env *e)
{
	int x;
	int y;
	int a;

	e->managed_text = NULL;
	if (!init_win("Slav 3D", WIN_WIDTH, WIN_HEIGHT, e))
		return (FALSE);
	a = SDL_SetHint (SDL_HINT_RENDER_SCALE_QUALITY, "1");
	printf("a = %d\n", a);
	if (!(e->screen_buf = create_texture(WIN_WIDTH, WIN_HEIGHT, e)))
		return (FALSE);
	init_togs(e);
	e->redx_texture = generate_redx_texture(TEXTURE_SIZE, e);
	e->greenx_texture = generate_greenx_texture(TEXTURE_SIZE, e);
	e->bluex_texture = generate_bluex_texture(TEXTURE_SIZE, e);
	e->grayx_texture = generate_grayx_texture(TEXTURE_SIZE, e);
	//textures passed here as Uint32 pointers, but definition is rly low !!
	e->textures[0] = png2surf("textures/wall1.png");//e->redx_texture;
	e->textures[1] = png2surf("textures/wall3.png");//e->greenx_texture;
	e->textures[2] = png2surf("textures/wall4.png");//e->bluex_texture;
	e->textures[3] = png2surf("textures/wall3.png");//e->grayx_texture;
	e->textures[4] = png2surf("textures/gopnik5.png");//e->grayx_texture;
	e->colors[0] = RGB_TO_ARGB(255, 0, 0);
	e->colors[1] = RGB_TO_ARGB(0, 255, 0);
	e->colors[2] = RGB_TO_ARGB(0, 0, 255);
	e->colors[3] = RGB_TO_ARGB(128, 128, 128);
	x = -1;
	while (++x < WIN_WIDTH)
	{
		y = -1;
		while(++y < WIN_HEIGHT)
			e->screen_buf[(WIN_WIDTH * y) + x] = 0xFFAAAAAA;

	}
	return (TRUE);
}
