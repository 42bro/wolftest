/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 18:04:06 by almoraru          #+#    #+#             */
/*   Updated: 2019/03/28 16:02:37 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

/*
** Dynamic memory blocks. In case the block is too full this function will copy
** a block's conent into a new one that is double it's size then free the first
 ** block \[T]/ 5 times faster than normal dynamic allocation
*/

void	*ft_mem(t_mem *mem, size_t size)
{
	void *p;

	if (mem->tsize < (size + mem->usize))
	{
		if ((p = ft_memalloc((2 * mem->tsize) + size + mem->usize)) == NULL)
			ft_error("Reallocation Failed!");
		mem->tsize = ((2 * mem->tsize) + size + mem->usize);
		if (mem->m != NULL)
		{
			ft_memcpy(p, mem->m, mem->usize);
			free(mem->m);
		}
		mem->m = p;
	}
	p = mem->m + mem->usize;
	mem->usize = mem->usize + size + 1;
	return (p);
}
