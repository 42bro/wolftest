/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slav3d.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 19:41:58 by almoraru          #+#    #+#             */
/*   Updated: 2019/04/11 19:50:01 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

/*
** Prepare your brain this is gonna get tricky
** PS: I hate the norm
** We hiding data in poinmters now boOOOOOOOOOOOOOOOOooooooooooooooooooooy
** Basically this will allow us to worry only about the data we want
** This will work for Doom as well btw \[T]/
*/


void *create_texture(unsigned int width, unsigned int height, t_env *e)
{
	Uint32 *data;
	t_texture *text;

	if (!width || !height || !e->renderer)
		ft_error("SDL hasn't been initliazed yet!");
	text = (t_texture *)ft_mem(e->mem, sizeof(t_texture));
	//text = (t_texture *)malloc(sizeof(t_texture));
	text->pitch = width * sizeof(Uint32);
	text->next = NULL;
	text->prev = NULL;
	text->tag = TEXTURE_ADDR;
	if(!(text->texture = SDL_CreateTexture(e->renderer, SDL_PIXELFORMAT_ARGB8888
										   ,SDL_TEXTUREACCESS_STREAMING
										   ,width ,height)))
	{
		free(e->mem->m);
		ft_error("Failed to make texture");
	}
	data = (Uint32 *)ft_mem(e->mem, (sizeof(Uint32) * width * height) + sizeof(t_texture *));
	//data = (Uint32 *)malloc((sizeof(Uint32) * width * height) + sizeof (t_texture *));
	*((t_texture**)data)= text;
	text->pixel_data = ((t_texture**)data) + 1;
	if (e->managed_text)
	{
		text->next = e->managed_text;
		e->managed_text->prev = text;
	}
	e->managed_text = text;

	return(text->pixel_data);

}

void	clear_renderer(t_env *e)
{
	SDL_RenderClear(e->renderer);
}


void	prep_fill_rect1(t_rect *rect, int x, int y)
{
	rect->x = x;
	rect->y = y;
}

void	prep_fill_rect2(t_rect *rect, int w, int h)
{
	rect->w = w;
	rect->h = h;
}

void	fill_rect(t_env *e, t_rect *recto)
{
	SDL_Rect rect;
	rect.x = recto->x;
	rect.y = recto->y;
	rect.w = recto->w;
	rect.h = recto->h;
	SDL_RenderDrawRect(e->renderer, &rect);
}

void	prep_draw_line1(t_point *p, int x1, int y1)
{
	p->x1 = x1;
	p->y1 = y1;
}

void	prep_draw_line2(t_point *p, int x2, int y2)
{
	p->x2 = x2;
	p->y2 = y2;
}

void	draw_line(t_env *e, t_point *p)
{
	int	x_offset;
	int	y_offset;

	x_offset = 0;
	y_offset = 0;
	if (p->x2 - p->x1 > 0)
		x_offset = -1;
	if (p->y2 - p->y1 > 0)
		y_offset = -1;
	SDL_RenderDrawLine(e->renderer, p->x1, p->y1
					   , p->x2 + x_offset, p->y2 + y_offset);
}
void	present_renderer(t_env *e)
{
	SDL_RenderPresent(e->renderer);
}

void	slav_draw_full_screen_texture(t_env *e, void *texture)
{
	t_texture *t;


	if (!e->win || !e->renderer)
		ft_error("SDL window has not beed initialized yet");
	t = *(((t_texture**)texture) - 1);
	if (t->tag != TEXTURE_ADDR)
		ft_error("Not a valid texture pointer");
	SDL_UpdateTexture(t->texture, NULL, t->pixel_data, t->pitch);
	clear_renderer(e);
	SDL_RenderCopy(e->renderer, t->texture, NULL, NULL);
	present_renderer(e);

}
