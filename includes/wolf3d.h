/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 16:56:09 by fdubois           #+#    #+#             */
/*   Updated: 2019/04/11 20:03:44 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H
// TODO(almoraru): Move texture_utils into the new engine name SLAV_3D
# include "../libft/libft.h"
# include "png.h"
# include <math.h>
# include <stdio.h>
//# include "../CL/cl.h"
# include <SDL2/SDL.h>
# include <SDL2/SDL_opengl.h>
# include <SDL2/SDL_mixer.h>

/*
** Window params
*/ 
# define WIN_WIDTH 1024
# define WIN_HEIGHT 580
// TODO(almoraru): Change map defines once I figure out how I want to handle map
// TODO(almoraru): Add racasting and projection parameters + map constants

/*
** Used to identify a texture structure in memory
*/
# define TEXTURE_ADDR		0x55AA

# define TRUE				1
# define FALSE				0
# define PI				3.141592653589 //bla bla can add more numbers if you want more precision 
# define EPS				0.00000001
/*
** Raycaster parameters
*/

# define FOV				(PI / 3.0f)
# define TEXTURE_SIZE		(128 * 2)
# define WALL_SIZE			(128 * 2)
# define PLAYER_SIZE (40 * 2)
# define PLAYER_MOVEMENT_SPEED 50.0f
# define PLAYER_ROT_SPEED ((4.2f * (PI)) / 180.0f)

/*
** Misc constants
*/

# define RGB_TO_ARGB(R, G, B)	(0xFF000000 | ((R) << 16) | ((G) << 8) | (B))
# define XY_TO_SCREEN_INDEX(X, Y)   (((Y) * WIN_WIDTH) + (X))
# define XY_TO_TEXTURE_INDEX(X, Y, Z)   (((Y) * Z) + (X))
# define DARKEN_COLOR(C)     ((((C) >> 1) & 0x7F7F7F7F) | 0xFF000000)


/*
** Special raycast settings
*/

# define ONLY_NORMAL 1
# define ONLY_FIRST_HIT 2

/*
** Projection params
*/

# define VIEWPLANE_LENGTH	WIN_WIDTH
# define VIEWPLANE_DIR_X	-1
# define VIEWPLANE_DIR_Y	0
# define PLAYER_DIR_X	0
# define PLAYER_DIR_Y	1
# define PLAYER_START_X	(2.5f * WALL_SIZE)
# define PLAYER_START_Y	(2.5f * WALL_SIZE)

/*
 ** Map wall types 
*/

# define P            -1  /* Player start */
# define R             1  /* Red wall */
# define G             2  /* Green wall */
# define B             3  /* Blue wall */
# define W             4  /* Gray wall */
# define A             5
# define CEILING_COLOR  RGB_TO_ARGB(0xAA, 0xAA, 0xFF)
# define FLOOR_COLOR    RGB_TO_ARGB(0x55, 0x55, 0x55)
# define MAP_GRID_WIDTH    10
# define MAP_GRID_HEIGHT   10
# define M_PIXEL_W   (e->mapw * WALL_SIZE)
# define M_PIXEL_H  (e->maph * WALL_SIZE)

/*
** Math utilities
*/

# define MAKE_FLOAT_EPS(A)  ((fabs((A)) < EPS) ? EPS : A) /* Make any value less than epsilon equal to epsilon */
# define RAY_EPS (WALL_SIZE / 3.0f)

typedef struct	s_mem
{
	void		*m;
	size_t	usize;
	size_t	tsize;

}				t_mem;

typedef struct	s_3f_vector
{
	float	x;
	float	y;
	float	z;
}				t_3f_vector;

typedef struct	s_player
{
	signed char	**map;
	char 		move_forward;
	char 		move_back;
	char		turn_left;
	char		turn_right;
	char		is_running;
	t_3f_vector	pos;
	t_3f_vector	dir;
	float		move_speed;
	int			x1;
	int			x2;
	int			y1;
	int			y2;
	int			mapw;
	int			maph;
}				t_player;

typedef float	t_matrix3f[3][3];

typedef struct	s_ray
{
	t_3f_vector	v_ray;
	t_3f_vector	h_ray;
}				t_ray;

typedef struct	s_ray_cast
{
	t_3f_vector v_norm;
	t_3f_vector h_norm;
	t_3f_vector v_step;
	t_3f_vector h_step;
	t_3f_vector map_coord;
}				t_ray_cast;

typedef enum	e_raytype
{
	h_ray,
	v_ray
}				e_raytype;

/*
**--------------------------------------------------------------------------------------
** SDL textures are stored in VRAM. We have to make a RAM-persistent
**--------------------------------------------------------------------------------------
** copy of the pixel data so we can write to it. I'll store it in a
**--------------------------------------------------------------------------------------
** linked list so it's easy to keep track of.
**--------------------------------------------------------------------------------------
*/
typedef struct	s_texture
{
	void			*pixel_data;
	SDL_Texture	*texture;
	Uint32	pitch;
	struct s_texture *next;
	struct s_texture *prev;
	Uint16	tag;
}				t_texture;

typedef struct	s_tog
{
	char	game_is_running;
	char	show_map;
	char	distortion;
	char	slow_render_mode;
	char	ray_cast_mode;
	char	texture_mode;
}				t_tog;
/*
**--------------------------------------------------------------------------------------
** Here is the env struct that will deal with the window creation and window management
**--------------------------------------------------------------------------------------
** Removed most MLX stuff since we'll use SDL
**--------------------------------------------------------------------------------------
** Since we're dealing with textures + colors I'll be using 32 unsigned ints
**--------------------------------------------------------------------------------------
*/
typedef struct	s_rect
{
	int	x;
	int	y;
	int	w;
	int	h;
}				t_rect;

typedef struct	s_point
{
	int	x1;
	int	y1;
	int	x2;
	int	y2;
}				t_point;

typedef struct	s_rend
{
	int	texture_x;
	int	map_x;
	int	map_y;
	SDL_Surface *texture;
	t_3f_vector	ray;
	e_raytype	rtype;
	t_3f_vector	coords;
	float	draw_len;
}				t_rend;

typedef struct	s_draw
{
	float	wall_y_start;
	Uint32	argb_color;
	SDL_Surface *texture;
	char	darken;

}				t_draw;

typedef struct	s_env
{
	t_mem		*mem;
	signed char	**map;
	t_tog		tog;
	t_player	player;
	t_rend	rend;
	t_draw	d;
	t_3f_vector	viewplane_dir;
	t_matrix3f counter_clock_rotation;
	t_matrix3f clock_wise_rotation;
	t_texture *managed_text;
	SDL_Window	*win;
	SDL_Surface	*surface;
	SDL_Event	event;
	SDL_Renderer *renderer;
	SDL_Surface		*textures[5];
	Uint32		*redx_texture;
	Uint32		*greenx_texture;
	Uint32		*bluex_texture;
	Uint32		*grayx_texture;
	Uint32		*screen_buf;
	Uint32		colors[4];
	int		maph;
	int		mapw;
	int			redmask;
	int			greenmask;
	int			bluemask;
	float		dist_from_viewplane;
}				t_env;
/* temporary map*/
extern const int MAP[MAP_GRID_HEIGHT][MAP_GRID_WIDTH];

/*
** Init functions
*/
void	init_ray_direction(t_env *e, t_ray *r);
int		ft_init(t_env *e);
int		init_win();
void	init_matrix(t_env *e);
void	init_player(t_player *p, t_env *e);
void	*create_texture(unsigned int width, unsigned int height, t_env *e);
Uint32	*generatex_texture(int size, t_env *e);
Uint32	*generate_redx_texture(int size,t_env *e);
Uint32	*generate_greenx_texture(int size,t_env *e);
Uint32	*generate_bluex_texture(int size, t_env *e);
Uint32	*generate_grayx_texture(int size, t_env *e);
/*
** Parsing functions
*/
void	parse(t_env *e, int ac, char **av);
/*
** Misc functions for handling keypress events etc
*/
int		clip_check(t_player *p, float dx, float dy);
void	ft_error(char *s);
void	move_player(t_player *p, float dx, float dy);
void	handle_sdl_events(t_env *e);
void	run_game(t_env *e);
void	*ft_mem(t_mem *mem, size_t size);

/*
** Renderer Functions
*/

void	render(t_env *e, t_ray *r);
void	clear_renderer(t_env *e);
void	render_map(t_env *e, int row, t_ray *r);
void	render_rays(t_env *e, t_ray *r, int map_x_offset, int map_y_offset);
void	present_renderer(t_env *e);
float	get_undistorted_ray_len(t_3f_vector *ray, t_3f_vector view_plane_dir);
void	slav_draw_full_screen_texture(t_env *e, void *texture);
int		get_texture_column_nb_for_ray(t_3f_vector *ray, e_raytype rtype, t_env *e);

/*
** Raycasting functions
*/
void	ray_cast(t_env *e, t_ray *r, int i);
t_3f_vector	get_tile_coord_for_v_ray(t_3f_vector *ray, t_env *e);
t_3f_vector	get_tile_coord_for_h_ray(t_3f_vector *ray, t_env *e);
t_3f_vector	find_v_ray_step_vec(t_3f_vector *ray);
t_3f_vector	find_h_ray_step_vec(t_3f_vector *ray);
/*
** Update functions
*/

void	update_player(t_env *e);
void	update_raycaster(t_env *e, t_ray *r);
/*
** Drawing functions
*/

void	prep_fill_rect1(t_rect *rect, int x, int y);
void	prep_fill_rect2(t_rect *rect, int w, int h);
void	fill_rect(t_env *e, t_rect *recto);
void	prep_draw_line1(t_point *p, int x1, int y1);
void	prep_draw_line2(t_point *p, int x2, int y2);
void	draw_line(t_env *e, t_point *p);
void	prerp_draw_no_texture(t_draw *d, float wall_y_start, Uint32 argb_color
						   , char darken);
void	draw_no_texture(int x, int len, t_draw *d, t_env *e);
float	calculate_draw_height(float ray_lenght, float distance);
void	draw_texture(int x, int len, t_draw *d, t_env *e);
void	prep_draw_texture(t_draw *d, float wall_y_start, SDL_Surface *texture, char darken);

/*
** Math functions for vector operations
*/

void	matrix_vector_multiply(t_matrix3f *mat, t_3f_vector *vec);
float	homogeneous_vector_magnitude(t_3f_vector *vec);
float	vector_point_product(t_3f_vector *vec1, t_3f_vector *vec2);
t_3f_vector	vector_subtract(t_3f_vector *vec1, t_3f_vector *vec2);
t_3f_vector	homogeneous_v_scale(t_3f_vector *vec, float scale);
t_3f_vector	normalize_vec(t_3f_vector *vec);
t_3f_vector	vector_add(t_3f_vector *vec1, t_3f_vector *vec2);
t_3f_vector	vector_proj(t_3f_vector *vec1, t_3f_vector *vec2);

SDL_Surface *png2surf(char *path);



#endif
