/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/23 03:07:18 by fdubois           #+#    #+#             */
/*   Updated: 2019/04/10 09:40:06 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PNG_H
# define PNG_H

# include "../libft/libft.h"
# include <stdlib.h>
# include <stdint.h>
# include <zlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <math.h>

# define WX 800
# define WY 600

# define MAX_IDAT_SIZE 1048576

typedef struct	s_raw
{
	struct s_raw	*next;
	unsigned char	*data;
}				t_raw;

typedef struct	s_mlx
{
	void	*mlx;
	void	*win;
	void	*img;
	int		*pix;
}				t_mlx;

typedef struct	s_img
{
	int			*plte;
	uint32_t	w;
	uint32_t	h;
	uint32_t	linesize;
	uint8_t		depth;
	uint8_t		color_type;
	uint8_t		compression;
	uint8_t		filter;
	uint8_t		interlaced;
	uint8_t		channels;
	uint8_t		bpp;
}				t_img;

void			sub_unfilter(unsigned char *str, uint32_t i, t_img img);
void			up_unfilter(unsigned char *str, uint32_t i, t_img img);
void			average_unfilter(unsigned char *str, uint32_t i, t_img img);
void			paeth_unfilter(unsigned char *str, uint32_t i, t_img img);
uint32_t		get_chunk_len(unsigned char *tmp);
void			crc_check(int fd, unsigned char *tmp);
void			read_for_nuthin(int fd, uint32_t len);
t_img			png_header_check(int fd);
t_img			get_png_header_info(int fd, int len);
void			gros_fail(char *str);

#endif
