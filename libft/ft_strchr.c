/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 18:05:34 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/10 18:46:47 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char		tar;
	size_t		i;

	i = 0;
	tar = (char)c;
	while (s[i] != '\0')
	{
		if (s[i] == tar)
			return (&((char*)s)[i]);
		i++;
	}
	if (s[i] == tar)
		return (&((char*)s)[i]);
	return (NULL);
}
