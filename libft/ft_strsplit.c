/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 13:37:09 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/10 20:37:23 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static char		**ft_put_words(char **tab, char const *s, size_t i, char c)
{
	size_t		j;
	int			start;
	int			end;

	j = 0;
	while (s[i] != '\0')
	{
		while (s[i] != '\0' && s[i] == c)
			i++;
		start = i;
		while (s[i] != '\0' && s[i] != c)
			i++;
		end = i;
		if (end > start)
			if (!(tab[j++] = ft_strsub(s, start, (end - start))))
			{
				free(tab[j--]);
				free(tab);
				return (NULL);
			}
	}
	tab[j] = NULL;
	return (tab);
}

char			**ft_strsplit(char const *s, char c)
{
	size_t		i;
	char		**tab;

	if (!(s && c))
		return (NULL);
	i = 0;
	if (!(tab = (char **)malloc(ft_strlen(s) * sizeof(char *))))
		return (NULL);
	tab = ft_put_words(tab, s, i, c);
	if (tab == NULL)
		return (NULL);
	return (tab);
}
